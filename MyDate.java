public class MyDate {
	private int day;
	private int month;
	private int year;

	
	public MyDate() {
		super();
		day = 0;
		month = 0;
		year = 0;
	}

	public MyDate(MyDate d) {
		super();
		if (d != null) {
			this.day = d.day;
			this.month = d.month;
			this.year = d.year;
		}
	}

	public void setDay(int day) {
		if (checkDay(day))
			this.day = day;
		else
			System.out.println("value of day is error");
	}

	public void setMonth(int month) {
		if (checkMonth(month))
			this.month = month;
		else
			System.out.println("value of month is error");
	}

	public void setYear(int year) {
		if (checkYear(year))
			this.year = year;
		else
			System.out.println("value of year is error");
	}

	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}


	
	public boolean checkDay(int day) {
		if (day >= 1 && day <= 30)
			return true;
		return false;
	}

	
	public boolean checkMonth(int month) {
		if (month >= 1 && month <= 12)
			return true;
		return false;
	}

	
	public boolean checkYear(int year) {
		if (year > 0)
			return true;
		return false;
	}

	
	@Override
	public String toString() {
		return "date of time : " + day + "/" + month + "/" + year;
	}

	public static void main(String[] args) {
		MyDate day1 = new MyDate();
		day1.setDay(1);
		day1.setMonth(1);
		day1.setYear(1995);

		System.out.println("day 1 : " + day1.toString());
		MyDate day2 = new MyDate(day1);
		System.out.println("day 2 : " + day2.toString());
	}

}
